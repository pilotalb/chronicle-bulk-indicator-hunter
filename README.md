# Chronicle Bulk Indicator Hunter

```$ python chronicle-bulk-hunt.py -h
usage: chronicle-bulk-hunt.py [-h] [-i IOC] [-f <FILE>]

optional arguments:
  -h, --help                  show this help message and exit
  -i IOC, --ioc IOC           list assets that have accessed an IOC
  -f <FILE>, --file <FILE>    search for IOCs from a file
