# Imports required for the sample - Google Auth and API Client Library Imports
# Get these packages from https://pypi.org/project/google-api-python-client/
# or run $ pip install google-api-python-client from your terminal
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import argparse
import datetime
import json
import re
import sys
import time
from googleapiclient import _auth
from google.oauth2 import service_account


# Build HTTP client for calling API
scopes = ['https://www.googleapis.com/auth/chronicle-backstory']
service_account_file = ('bk_credentials.json')
credentials = service_account.Credentials.from_service_account_file(
    service_account_file, scopes=scopes)
http_client = _auth.authorized_http(credentials)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--ioc', help="""list assets that have accessed
        an IOC""")
    parser.add_argument('-f', '--file', action='append', nargs=1, metavar=('<FILE>'),
                      help='Search for IOCs from a file')
    args = parser.parse_args()

    if args.ioc:
        call_list_assets("")
    if args.file:
        call_ioc_file()


def call_ioc_file():
    f = open(sys.argv[2], 'r')
    for line in f:
        #Clean up any newlines or spaces
        line = line.replace('\n','')
        line = line.replace('\s', '')
        # The API handles 1 QPS, so let's puase for
        #  1 second to avoid resource errors.
        time.sleep(1)
        call_list_assets(line)


def call_list_assets(ioc_cmd):
    # The API support searching by IP, domain name, md5, sha1, sha256
    #  Let's use regex to determine which type of IOC we received, as we'll
    #  need this in the API call
    if (ioc_cmd == ""):
        ioc = sys.argv[2]
    else:
        ioc = ioc_cmd
    md5 = re.match('^[a-fA-F0-9]{32}$', ioc)
    sha1 = re.match('^[a-fA-F0-9]{40}$', ioc)
    sha256 = re.match('^[a-fA-F0-9]{64}$', ioc)
    ipaddr = re.match(r'^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}'
                        '([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$', ioc)
    domain = re.match(r'^(?=.{1,255}$)(?!-)[A-Za-z0-9\-]{1,63}(\.[A-Za-z0-9\-]'
                    r'{1,63})*\.?(?<!-)$', ioc)

    if md5:
        ioc_api = 'hash_md5'
    elif sha1:
        ioc_api = 'hash_sha1'
    elif sha256:
        ioc_api = 'hash_sha256'
    elif ipaddr:
        ioc_api = 'destination_ip_address'
    elif domain:
        ioc_api = 'domain_name'
    else:
        print('Not a valid/supported IOC: ' + ioc)
        return

    # Get the current zulu time for polling the API
    end_time = datetime.datetime.utcnow()
    end_time = end_time.strftime('%Y-%m-%dT%H:%M:00Z')

    # Construct the URL
    backstory_api_v1_url = 'https://backstory.googleapis.com/v1'
    list_assets_url = (backstory_api_v1_url + '/artifact/listassets?'
                     'start_time=2018-01-01T00:00:00Z&end_time=' + end_time +
                     '&artifact.' + ioc_api + '=' + ioc)

    # Track the time it takes to execute the query
    start_time = time.time()

    # Make a request
    response = http_client.request(list_assets_url, 'GET')

    # Parse the response
    if response[0].status == 200:
        iocs = response[1]
        # List of IoCs are returned for further processing
        print("\n")
        print(ioc + ": ", end = '')
        print_formatted(iocs)
    else:
        # something went wrong, please see the response detail
        err = response[1]
        print(err)
    elapsed_time = time.time() - start_time
    if (ioc_cmd == ""):
        print('\nQuery time (secs): ' + format(elapsed_time))


def print_formatted(contents):
    # Check to see if we have matches
    if 'assets' not in contents:
        print('Indicator not found\n')
        return

    # Load the API output as JSON
    js = json.loads(contents)

    # Loops through the matches
    for devices in js['assets']:
        jsonLine = str(devices['asset'])
        jsonLine = jsonLine.split("\'")
        print(jsonLine[3] + ",", end = '')


if __name__ == '__main__':
    main()